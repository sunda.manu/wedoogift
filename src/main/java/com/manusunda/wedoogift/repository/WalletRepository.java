package com.manusunda.wedoogift.repository;

import com.manusunda.wedoogift.model.Wallet;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by manu_ on 15/03/2021.
 */
@Repository
public interface WalletRepository extends CrudRepository<Wallet, Long> {

    Wallet findByType(String Type);
}
