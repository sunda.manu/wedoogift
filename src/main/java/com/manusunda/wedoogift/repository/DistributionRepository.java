package com.manusunda.wedoogift.repository;

import com.manusunda.wedoogift.model.Distribution;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by manu_ on 13/03/2021.
 */
@Repository
public interface DistributionRepository extends CrudRepository<Distribution, Long> {}
