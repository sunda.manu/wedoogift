package com.manusunda.wedoogift.repository;

import com.manusunda.wedoogift.model.Company;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by manu_ on 13/03/2021.
 */
@Repository
public interface CompanyRepository extends CrudRepository<Company, Long>{

    @Query("SELECT c.id FROM Company c")
    Iterable<Long> getAllIds();


}
