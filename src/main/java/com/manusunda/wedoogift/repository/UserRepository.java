package com.manusunda.wedoogift.repository;

import com.manusunda.wedoogift.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by manu_ on 13/03/2021.
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    Iterable<User> findUserByCompanyId(Long companyId);

    @Query("SELECT sum(amount) FROM Distribution where startDate <= CURRENT_DATE  AND endDate >= CURRENT_DATE AND userId=?1 AND walletId=?2")
    Optional<Long> getDistribuedAmountForWalletId(long userId, long walletId);



}
