package com.manusunda.wedoogift.exception;

/**
 * Created by manu_ on 16/03/2021.
 */
public class DataNotFoundException extends RuntimeException {
    public DataNotFoundException(Long id) {

        super(String.format("Data with Id %d not found", id));
    }
}
