package com.manusunda.wedoogift.exception;

/**
 * Created by manu_ on 16/03/2021.
 */
public class NoDataCreatedException extends RuntimeException  {

    public NoDataCreatedException() {
        super(String.format("No data created"));
    }
}
