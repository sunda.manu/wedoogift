package com.manusunda.wedoogift.exception;

/**
 * Created by manu_ on 16/03/2021.
 */
public class NoDataFoundException extends RuntimeException  {

    public NoDataFoundException() {
        super(String.format("No data found"));
    }
}
