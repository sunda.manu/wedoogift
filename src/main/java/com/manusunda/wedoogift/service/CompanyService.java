package com.manusunda.wedoogift.service;

import com.manusunda.wedoogift.CustomProperties;
import com.manusunda.wedoogift.exception.NoDataCreatedException;
import com.manusunda.wedoogift.exception.NoDataFoundException;
import com.manusunda.wedoogift.model.Company;
import com.manusunda.wedoogift.model.Distribution;
import com.manusunda.wedoogift.model.User;
import com.manusunda.wedoogift.model.Wallet;
import com.manusunda.wedoogift.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

/**
 * Created by manu_ on 13/03/2021.
 */
@Service
public class CompanyService {

    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    UserService userService;

    @Autowired
    DistributionService distributionService;

    @Autowired
    WalletService walletService;

    @Autowired
    CustomProperties props;

    public List<Company> saveAll(List<Company> companies){
        List<Company> savedCompanies = new ArrayList<>();

        companyRepository.saveAll(companies).forEach(savedCompanies::add);

        return companies;
    }

    public Company save(Company company){

        companyRepository.save(company);

        return company;
    }

    public List<Company> getAll(){
        List<Company> companies = new ArrayList<>();

        companyRepository.findAll().forEach(companies::add);

        if(companies.size() == 0){
            throw new NoDataFoundException();
        }

        return companies;
    }


    public List<Distribution> distribute(long companyId, int amount, String walletType){
        List<Distribution> distributions = null;
        Optional<Company> maybeCompany = companyRepository.findById(companyId);
        Company company;
        List<User> companyUsers;

        Wallet wallet = walletService.findWalletFromType(walletType);

        Distribution distribution;

        if(maybeCompany.isPresent()){
            company = maybeCompany.get();
            companyUsers = userService.getUsersFromCompany(companyId);

            //Vérification possibilité de distribution
            //La balance entreprise est recalculé apres chaque distribution, donc pas besoin de mettre à jour la balance avant verif
            if(company.getBalance() > amount*companyUsers.size() && amount > 0 && companyUsers.size() > 0 && wallet != null ){
                distributions = new ArrayList<>();
                for (User user: companyUsers) {
                    //Création de la distribution
                    distribution = new Distribution();
                    distribution.setCompanyId(companyId);
                    distribution.setUserId(user.getId());
                    distribution.setWalletId(wallet.getId());
                    distribution.setAmount(amount);
                    distribution.setStartDate(new Date(System.currentTimeMillis()));
                    distribution.setEndDate(getEndDateFromStartDate(distribution.getStartDate(), walletType));
                    distributions.add(distribution);

                }

                //Enregistrements des distributions en bdd
                distributions = distributionService.saveAll(distributions);

                //Mise à jour de la balance de l'entreprise
                updateCompanyBalanceAfterDistribution(company, amount*companyUsers.size());


            }else{
                System.out.println("Distribution impossible");
            }
        }else{
            System.out.println("Distribution impossible, la société avec l'id " + companyId + " n'éxiste pas");
        }

        if(distributions == null || distributions.size() == 0){
            throw new NoDataCreatedException();
        }

        return distributions;
    }


    /**
     * Mise à jour de la balance d'une entreprise après une distribution.
     * On part du principe que les utilisateurs utiliseront la totalité de leurs carte cadeau.
     * Donc pas besoin d'additionner les sommes non utilisées pour les redonner à l'entreprise
     *
     * @param company
     * @param amount
     */
    private void updateCompanyBalanceAfterDistribution(Company company, int amount) {
        company.setBalance(company.getBalance() - amount);
        save(company);
    }

    private Date getEndDateFromStartDate(Date startDate, String walletType){
        Date endDate = null;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);

        if(walletType.equals(props.getGiftCardType())) {
            //Durée de vie d'une carte cadeau 365j
            calendar.add(Calendar.DATE, 365);

            endDate = new Date(calendar.getTimeInMillis());
        }else if(walletType.equals(props.getMealVoucherType())){
            //Fin de vie d'un ticket resto, dernier jour du mois de février de l'année suivante
            calendar.add(Calendar.DATE, 365);
            calendar.set(Calendar.MONTH, 1);
            calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));

            endDate = new Date(calendar.getTimeInMillis());
        }

        return endDate;

    }


    public List<Long> getAllIds(){
        List<Long> allIds = new ArrayList<>();
        companyRepository.getAllIds().forEach(allIds::add);

        if(allIds == null || allIds.size() == 0){
            throw new NoDataFoundException();
        }

        return allIds;
    }
}
