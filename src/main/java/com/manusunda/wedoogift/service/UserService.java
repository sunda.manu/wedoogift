package com.manusunda.wedoogift.service;

import com.manusunda.wedoogift.exception.DataNotFoundException;
import com.manusunda.wedoogift.exception.NoDataFoundException;
import com.manusunda.wedoogift.model.Balance;
import com.manusunda.wedoogift.model.User;
import com.manusunda.wedoogift.model.Wallet;
import com.manusunda.wedoogift.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by manu_ on 13/03/2021.
 */

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private DistributionService distributionService;

    @Autowired
    private WalletService walletService;

    public List<User> getAll(){
        List<User> users = new ArrayList<>();

        userRepository.findAll().forEach(users::add);

        if(users == null || users.size() == 0){
            throw new NoDataFoundException();
        }

        return users;
    }

    public List<User> getUsersFromCompany(long companyId){

        List<User> users = new ArrayList<>();

        userRepository.findUserByCompanyId(companyId).forEach(users::add);

        return users;

    }

    public User getById(long id){
        User user = null;
        Optional<User> maybeUser = userRepository.findById(id);

        if (maybeUser.isPresent()){
            user = maybeUser.get();
        }
        if(user == null){
            throw new DataNotFoundException(id);
        }
        return user;
    }

    public User save(User user){
        User savedUser = userRepository.save(user);
        return savedUser;
    }

    public List<User> saveAll(List<User> users) {
        List<User> savedUsers = new ArrayList<>();
        userRepository.saveAll(users).forEach(savedUsers::add);
        return savedUsers;
    }

    /**
     * Assigner une entreprise au hasard à chaque utilisateur .
     * @return
     */
    public List<User> assignCompany(){
        List<User> users = new ArrayList<>();
        userRepository.findAll().forEach(users::add);

        List<Long> companiesId = companyService.getAllIds();


        for (User user : users) {
            int randomCompany = (int) ((Math.random() * (companiesId.size())));

            user.setCompanyId(companiesId.get(randomCompany));
        }

        saveAll(users);

        return users;
    }



    /**
     * Cette fonction va chercher la bonne balance pour l'utilisateur.
     * Si elle n'existe pas, elle est créée
     * @param walledId
     * @return
     */
    public Balance getBalanceFromWallet(long walledId, User user){
        Balance balance = null;

        for(Balance userBalance : user.getBalances()){
            if(userBalance.getWalletId() == walledId){
                balance = userBalance;
                break;
            }
        }

        if(balance == null){
            balance = createUserBalance(walledId, user);
        }

        return balance;
    }

    private Balance createUserBalance(long walletId, User user){
        Balance balance = null;

        if(user.getBalances().size() < walletService.count()) {
            balance = new Balance();
            balance.setWalletId(walletId);
            balance.setUser(user);
            balance.setAmount(0l);
            user.addBalance(balance);

        }else{
            System.out.println("Création de compte impossibe: l'utilisateur " + user.getId() + " à déja assez de comptes.");
        }

        return balance;
    }


    public long getBalanceAmountForUser(long userId){
        long amount = 0;

        User user = getById(userId);

        if(user != null) {

            for(Balance balance : user.getBalances() ){
                amount += balance.getAmount();
            }
        }

        return amount;
    }

    public void initBalance(List<User> users) {
        for(User user : users){
            for(Balance balance : user.getBalances()){
                balance.setUser(user);
                balance.setInitialAmount(balance.getAmount());
            }
        }
    }


    public List<User> refreshUserBalance(){
        List<User> users = getAll();
        List<Wallet> wallets = walletService.findAll();
        Balance balance;
        long balanceAmount = 0;
        Optional<Long> maybeAmount;

        for(User user : users){
            for(Wallet wallet: wallets){
                maybeAmount = userRepository.getDistribuedAmountForWalletId(user.getId(),wallet.getId());

                if(maybeAmount.isPresent()){
                    balanceAmount = maybeAmount.get();
                }


                if(balanceAmount != 0 ){
                    balance = getBalanceFromWallet(wallet.getId(), user);
                    balance.setAmount(balance.getInitialAmount() + balanceAmount);
                }

                balanceAmount = 0;
            }

        }

        users = saveAll(users);

        return users;
    }
}
