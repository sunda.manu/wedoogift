package com.manusunda.wedoogift.service;

import com.manusunda.wedoogift.exception.NoDataFoundException;
import com.manusunda.wedoogift.model.Wallet;
import com.manusunda.wedoogift.repository.WalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manu_ on 15/03/2021.
 */
@Service
public class WalletService {
    @Autowired
    private WalletRepository walletRepository;


    public long count(){
        long nb;

        nb = walletRepository.count();

        return nb;
    }

    public Wallet findWalletFromType(String type){

        Wallet wallet = walletRepository.findByType(type);

        return wallet;
    }


    public List<Wallet> saveAll(List<Wallet> wallets) {
        List<Wallet> savedWallet = new ArrayList<>();

        walletRepository.saveAll(wallets).forEach(savedWallet::add);

        return savedWallet;
    }

    public List<Wallet> findAll() {
        List<Wallet> wallets = new ArrayList<>();

        walletRepository.findAll().forEach(wallets::add);

        if(wallets == null || wallets.size() == 0){
            throw new NoDataFoundException();
        }

        return wallets;
    }
}
