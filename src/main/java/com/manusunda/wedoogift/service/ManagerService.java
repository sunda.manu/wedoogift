package com.manusunda.wedoogift.service;

import com.manusunda.wedoogift.model.Manager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by manu_ on 14/03/2021.
 */

@Service
public class ManagerService {

    @Autowired
    UserService userService;

    @Autowired
    CompanyService companyService;

    @Autowired
    DistributionService distributionService;

    public Manager updateManager(Manager manager){
        manager.setUsers(userService.getAll());
        manager.setCompanies(companyService.getAll());
        manager.setDistributions(distributionService.getAll());

        return manager;
    }


}
