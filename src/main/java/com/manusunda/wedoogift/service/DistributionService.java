package com.manusunda.wedoogift.service;

import com.manusunda.wedoogift.model.Distribution;
import com.manusunda.wedoogift.repository.DistributionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manu_ on 13/03/2021.
 */
@Service
public class DistributionService {

    @Autowired
    DistributionRepository distributionRepository;

    @Autowired
    UserService userService;

    public List<Distribution> saveAll(List<Distribution> distributions){
        List<Distribution> savedDistributions = new ArrayList<>();

        distributionRepository.saveAll(distributions).forEach(savedDistributions::add);
        return savedDistributions;
    }



    public List<Distribution> getAll(){
        List<Distribution> distributions = new ArrayList<>();

        distributionRepository.findAll().forEach(distributions::add);

        return distributions;
    }
}
