package com.manusunda.wedoogift;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by manu_ on 15/03/2021.
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "com.manusunda.wedoogift.custo")
public class CustomProperties {

    private String mealVoucherType;

    private String giftCardType;
}
