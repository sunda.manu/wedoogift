package com.manusunda.wedoogift.controller;

import com.manusunda.wedoogift.model.User;
import com.manusunda.wedoogift.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by manu_ on 13/03/2021.
 */

@Api( description="Gestion des utilisateur")
@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserService userService;

    public Iterable<User> saveAll(List<User> users) {
        return userService.saveAll(users);
    }


    public List<User> assignCompany(){
        return userService.assignCompany();
    }





    public void initBalance(List<User> users) {
        userService.initBalance(users);
    }

    public List<User> refreshUserBalance(){
        return userService.refreshUserBalance();
    }

    @ApiOperation(value = "Affichage des utilisateur (Refresh balance utilisateur inclus)")
    @GetMapping("/users")
    public List<User> getAll(){
        return refreshUserBalance();
    }


    @GetMapping("/user/{id}")
    public User getById(@PathVariable("id") long userId){
        return userService.getById(userId);
    }


}
