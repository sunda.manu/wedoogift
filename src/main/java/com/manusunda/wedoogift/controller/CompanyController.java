package com.manusunda.wedoogift.controller;

import com.manusunda.wedoogift.model.Company;
import com.manusunda.wedoogift.model.Distribution;
import com.manusunda.wedoogift.service.CompanyService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by manu_ on 13/03/2021.
 */

@RestController
@RequestMapping("/api")
public class CompanyController {

    @Autowired
    CompanyService companyService;

    @Autowired
    WalletController walletController;

    public List<Company> saveAll(List<Company> companies) {
        return companyService.saveAll(companies);
    }


    @ApiOperation(value = "Distribution")
    @GetMapping("company/{id}/distribute")
    public List<Distribution> distribute(@PathVariable("id") int companyId,@RequestParam("amount") int amount,@RequestParam("type") String walletType){

        List<Distribution> distributions;

        distributions = companyService.distribute(companyId, amount, walletType);

        return distributions;
    }

    @GetMapping("/companies")
    public List<Company> getAll(){
        return companyService.getAll();
    }
}
