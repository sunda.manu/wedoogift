package com.manusunda.wedoogift.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.manusunda.wedoogift.CustomProperties;
import com.manusunda.wedoogift.model.Manager;
import com.manusunda.wedoogift.service.ManagerService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by manu_ on 14/03/2021.
 */
@Api( description="Gestion du projet: initialisation et visualisation de l'export")
@RestController
@RequestMapping("/api")
public class ManagerController {


    @Autowired
    ManagerService managerService;

    @Autowired
    ManagerController managerController;

    @Autowired
    WalletController walletController;

    @Autowired
    UserController userController;

    @Autowired
    CompanyController companyController;

    @Autowired
    CustomProperties props;


    public Manager updateManager(Manager manager){
        manager = managerService.updateManager(manager);

        return manager;
    }

    @GetMapping("/init")
    public Manager initApi() {
        Manager manager = null;
        InputStream inputStream = TypeReference.class.getResourceAsStream("/json/input.json");
        ObjectMapper mapper = new ObjectMapper();

        TypeReference<Manager> typeReference = new TypeReference<Manager>() {};

        try {
            manager = mapper.readValue(inputStream, typeReference);
            userController.initBalance(manager.getUsers());
            userController.saveAll(manager.getUsers());
            companyController.saveAll((manager.getCompanies()));
            walletController.saveAll(manager.getWallets());


            System.out.println("Chargement des données JSON OK!");

            //Les users n'ont pas de company par defaut, donc assignation d'une company au hasard.
            userController.assignCompany();

            //Génération du fichier json export
            manager = managerController.updateManager(manager);


        } catch (IOException e) {
            System.out.println("Erreur de chargement fichier json : " + e.getMessage());
        }

        return manager;
    }


    @GetMapping("")
    public Manager getManager(){
        Manager manager = new Manager() ;

        manager = managerService.updateManager(manager);

        return manager;
    }
}
