package com.manusunda.wedoogift.controller;

import com.manusunda.wedoogift.CustomProperties;
import com.manusunda.wedoogift.model.Wallet;
import com.manusunda.wedoogift.service.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * Created by manu_ on 15/03/2021.
 */
@Controller
public class WalletController {

    @Autowired
    WalletService walletService;

    @Autowired
    CustomProperties props;

    public List<Wallet> saveAll(List<Wallet> wallets){
        return walletService.saveAll(wallets);
    }

}
