package com.manusunda.wedoogift.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import javax.persistence.*;
import java.sql.Date;
import java.text.SimpleDateFormat;

/**
 * Created by manu_ on 13/03/2021.
 */

@Data
@Entity
@Table(name = "distributions")
@JsonPropertyOrder({"id", "wallet_id", "amount", "start_date", "end_date", "company_id", "user_id"})
public class Distribution {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer amount;

    @JsonProperty("wallet_id")
    @Column(name="wallet_id")
    private Long walletId;

    @JsonIgnore
    @Column(name = "start_date")
    private Date startDate;

    @JsonIgnore
    @Column(name = "end_date")
    private Date endDate;

    @JsonProperty("company_id")
    @Column(name = "company_id")
    private Long companyId;

    @JsonProperty("user_id")
    @Column(name = "user_id")
    private Long userId;

    @JsonProperty("start_date")
    private String getStartDateString(){
        SimpleDateFormat sdf = new SimpleDateFormat(
                "yyyy-MM-dd");

        return sdf.format(startDate);
    }

    @JsonProperty("end_date")
    private String getEndDateString(){
        SimpleDateFormat sdf = new SimpleDateFormat(
                "yyyy-MM-dd");

        return sdf.format(endDate);
    }

}
