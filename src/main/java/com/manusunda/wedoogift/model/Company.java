package com.manusunda.wedoogift.model;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by manu_ on 13/03/2021.
 */

@Data
@Entity
@Table(name = "companies")
public class Company {

    //Id géré par input JSON donc pas d'auto incrémentation
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Integer balance;


}
