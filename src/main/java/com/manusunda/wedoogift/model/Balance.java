package com.manusunda.wedoogift.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import javax.persistence.*;

/**
 * Created by manu_ on 14/03/2021.
 */
@Data
@Entity
@Table(name="balances")
@JsonPropertyOrder({ "wallet_id", "amount"})
public class Balance {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;


    private Long amount;

    @Column(name="wallet_id")
    @JsonProperty("wallet_id")
    private Long walletId;


    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @JsonIgnore
    @Column(name="initial_amount")
    private Long initialAmount = 0l;

}
