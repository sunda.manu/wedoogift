package com.manusunda.wedoogift.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import java.util.List;

/**
 * Created by manu_ on 13/03/2021.
 */
@Data
@JsonPropertyOrder({ "companies", "users", "distributions" })
@JsonIgnoreProperties(allowSetters = true, value = {"wallets"})
public class Manager{

    private List<User> users;

    private List<Company> companies;

    private List<Distribution> distributions;

    private List<Wallet> wallets;

}
