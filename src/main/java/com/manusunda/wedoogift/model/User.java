package com.manusunda.wedoogift.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by manu_ on 13/03/2021.
 */
@Entity
@Data
@Table(name="users")
@JsonRootName(value = "users")
public class User {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user", cascade = CascadeType.ALL)
    @JsonProperty("balance")
    private List<Balance> balances;

    @JsonIgnore
    @Column(name="company_id")
    private Long companyId;

    public boolean addBalance(Balance balance){
        if(balances == null){
            balances = new ArrayList<>();
        }

        return balances.add(balance);
    }

}
