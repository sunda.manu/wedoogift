package com.manusunda.wedoogift.model;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import javax.persistence.*;

/**
 * Created by manu_ on 15/03/2021.
 */
@Data
@Entity
@Table(name="wallets")
@JsonRootName(value = "wallets")
public class Wallet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String type;
}
