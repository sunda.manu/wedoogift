package com.manusunda.wedoogift.Service;

import org.springframework.boot.test.context.SpringBootTest;

/**
 * Created by manu_ on 13/03/2021.
 */
@SpringBootTest
public class CompanyServiceTests {
/*
    @Autowired
    CompanyService companyService;

    @Autowired
    UserService userService;

    @Autowired
    CustomProperties props;

    @Test
    void testDistributeGiftCards(){
        User user = new User();
        user.setCompanyId(1l);

        userService.save(user);


        //Test avec une somme trop grande (la company n'a que 3000 euros), donc valeur null attendue
        List<Distribution> canDitribute = companyService.distribute(1, 30000, props.getGiftCardType());
        assertEquals(null, canDitribute);

        //Test avec une somme cohérente , une Iterable<Distribution> est attendu
        List<Distribution> canDitribute2 = companyService.distribute(1, 1, props.getGiftCardType());
        assertThat(canDitribute2).isInstanceOf(List.class);

        //Test avec un companyId n'existant pas en base
        List<Distribution> canDitribute3 = companyService.distribute(999, 30000, props.getMealVoucherType());
        assertEquals(null, canDitribute3);

        //Distribution de 0 euro, valeur null attendue
        List<Distribution> canDitribute4 = companyService.distribute(1, 0, props.getMealVoucherType());
        assertEquals(null, canDitribute4);

        //TODO Vérifier le bon fonctionnement des endDate
    }

    @Test
    void testGetAll(){
        List<Company> companies;
        companies = companyService.getAll();
        assertTrue(2<= companies.size());


        assertEquals("Wedoogift", companies.get(0).getName());
    }

    @Test
    void testSave(){

        Company company = new Company();
        company.setBalance(9000);
        company.setId(3l);
        company.setName("New Company");

        companyService.save(company);

        List<Company> companies = companyService.getAll();

        assertEquals(9000, companies.get(companies.size()-1).getBalance());

        //Todo: Recup une company existante, la modifier et l'enregistrer, verifier que la modif est bonne

    }

    @Test
    void testSaveAll(){

        Company company = null;
        List<Company> companies = new ArrayList<>();


        company = new Company();
        company.setBalance(9000);
        company.setName("New Company");
        companies.add(company);

        company = new Company();
        company.setBalance(2000);
        company.setName("New Company2");
        companies.add(company);

        company = new Company();
        company.setBalance(50);
        company.setName("New Company3");
        companies.add(company);

        companyService.saveAll(companies);

        List<Company> companiesTest = companyService.getAll();

        assertTrue(3 <= companiesTest.size());


        companies = new ArrayList<>();

        assertEquals(0, companies.size());

    }

*/
}
