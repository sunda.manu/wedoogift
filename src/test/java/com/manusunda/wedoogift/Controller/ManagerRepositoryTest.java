package com.manusunda.wedoogift.Controller;

import com.manusunda.wedoogift.controller.CompanyController;
import com.manusunda.wedoogift.controller.ManagerController;
import com.manusunda.wedoogift.controller.UserController;
import com.manusunda.wedoogift.controller.WalletController;
import com.manusunda.wedoogift.service.ManagerService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by manu_ on 16/03/2021.
 */
@WebMvcTest(controllers = ManagerController.class)
public class ManagerRepositoryTest {
    //TODO: Faire les tests UserRepositoryTest, CompanyRepositoryTest....


    @Autowired
    private MockMvc mockMvc;



    @MockBean
    ManagerService managerService;

    @MockBean
    ManagerController managerController;

    @MockBean
    WalletController walletController;

    @MockBean
    UserController userController;

    @MockBean
    CompanyController companyController;

    @Test
    public void testgetManager() throws Exception {
        mockMvc.perform(get("/api/"))
                .andExpect(status().isOk());
    }

    @Test
    public void testInitManager() throws Exception {
        mockMvc.perform(get("/api/init"))
                .andExpect(status().isOk());
    }
}
